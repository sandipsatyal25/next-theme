<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body>
<?php if(is_home()){?>
<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-dark nav-blog">
<a href="#" class="navbar-brand">Exp.</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
<?php wp_nav_menu(array('theme_location'=>'landing_page_primary'));?>
</div>
</nav>
<?php }
elseif(is_front_page()){?>
<div class="empty"></div>
<?php }
else{?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark nav-blog">
<a href="#" class="navbar-brand">Exp.</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent"> 
<?php wp_nav_menu(array('theme_location'=>'landing_page_primary'));?>
</div>
</nav>
<?php }
?> 
