<?php get_header();?>
<?php
if(have_posts()):
    while(have_posts()):the_post(); ?>
    <div class="whole-page">
    <video autoplay loop muted> 
    <source src="http://localhost/wordpress/wp-content/uploads/2018/12/for-wordpress.mp4" type="video/mp4">
    </video>  
    </div>
    <div class="front-page-wrapper" style="background-image:url('<?php the_post_thumbnail_url();?>')">
    <div class="container front-page-content">
    <?php the_content(); ?>
    </div>
    <div class="front-page-navbar-wrapper">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark nav-home">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <?php wp_nav_menu(array('theme_location'=>'landing_page_primary'));?>
    <ul class="search">
    <li>
        <form action="#" method="POST">
            <input type="search" placeholder="Search" class="home-search-input">
            <input type="submit" name="submit" value="search" class="home-search-input-submit">
        </form>
    </li>
    </ul>
</div>
    </nav>
    </div>

<?php endwhile;
endif;
get_footer();
wp_footer(); ?>
</div>
