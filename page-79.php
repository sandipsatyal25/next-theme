<?php get_header();?>
<?php if(have_posts()):
while(have_posts()):
    the_post();?>
    <div class="resume-header-wrapper" style="background-image:url('<?php echo the_post_thumbnail_url(); ?>')"> 
    <?php
    $query = array(
        'cat' => '32',
        'posts_per_page' => '1'
    );
    $exec = new WP_Query($query);
    if($exec->have_posts()):
        while($exec->have_posts()):
            $exec->the_post();?>
            <div class="resume-header-content">
            <div class="resume-header-content-thumbnail"><?php the_post_thumbnail();?></div>
            <div class="resume-header-content-dynamic-content"><?php the_content();?>
        <?php endwhile;
    endif;
    ?>   
    </div><!--resume-header-content-->
    </div><!--resume header-wrapper-->
<?php endwhile;
endif;
?>

<?php wp_footer();?>
<?php get_footer();?>