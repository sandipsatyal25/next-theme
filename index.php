<?php get_header(); ?>
   <?php
   $args = array(
       'post_type' => 'post',
       'cat' => '27',
       'posts_per_page' => 1,
       'post_status' => 'publish',
   );
   $exec = new WP_Query($args);
   if($exec -> have_posts()):
    while ($exec->have_posts()):
        $exec->the_post();?>
        <header class="blog" style="background-image:url('<?php echo the_post_thumbnail_url();?>')">
        <div class="container heading"><?php the_title(); ?></div>
        <div class="collapse container sub-heading" id="blog-header-content-wrapper"><?php the_content(); ?></div>
    <?php endwhile;
endif;
   ?>
   <div class="blog-button-wrapper">
   <a href="#blog-header-content-wrapper" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="blog-header-content-wrapper" id="headerShowMoreButton"> <div class="blog-button shadow-lg" id="showMoreButton"><span id="HeadershowMoreText">&#x238A;</span></div></a>   
    </div>
</header>
<div class="container blog-wrapper">
<div class="blog-wrapper-left">
<?php
$args2 = array(
    'cat' => '-26'
);
$exec2 = new WP_Query($args2);
if($exec2->have_posts()):
    while($exec2->have_posts()): 
        $exec2->the_post(); ?>
    <div class="blog-content-wrapper shadow-sm">
    <?php if(has_post_thumbnail()){ ?>
    <div class="blog-content-thumbnail">
    <?php the_post_thumbnail('blog_thumbnail');?>
    </div>
    <?php } ?>
    <div class="blog-content-title">
    <?php the_title();?>
    <div class="blog-content-meta-data">
    <span>Posted on: <?php the_date();?></span>
    <span class="pr-2 pl-2">Category:</span><?php the_category();?>
    <span>Posted by: <?php the_author();?></span>
    </div>
    </div>
    <div class="blog-content-data">
    <?php the_content(); ?>
    </div>
    </div>
<?php endwhile;
endif;
?>
</div>
<div class="blog-wrapper-right shadow-sm">
    <div class="widget-search-wrapper">
    <form action="#" method="POST" class="blog-search-form">
        <input type="search" placeholder="Search ">
        <input type="submit" value="&#128269;">
    </form>
    </div>
    <div class="widget-category-list-wrapper mt-5">
    <h5 class="title">Category</h5>
        <div class="widget-category-list">
        <?php
        $categories =  get_categories(array(
            'exclude' => array(26,27,29,32)
        ));  
        foreach  ($categories as $category) {
            //Display the sub category information using $category values like $category->cat_name
            echo '<div class="category-item">'.'<a href="#" class="no-decoration">'.$category->name.'</a></div>';
           }
        ?>
        </div>
    </div>
    <div class="widget-social-wrapper mt-5">
    <h5 class="title">Social</h5>    
    <div class="widget-social">
           <ul class="social">
               <li>Facebook</li>
               <li>Twitter</li>
               <li>Youtube</li>
               <li>LinkedIn</li>
           </ul>
        </div>
    </div>
</div>
</div>

<?php wp_footer();?>
<?php get_footer(); ?>
