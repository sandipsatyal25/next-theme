<?php
@ini_set( 'upload_max_size', '12M' );
@ini_set( 'post_max_size', '64M' );
@ini_set( 'max_execution_time', '300' );

function next2_script_enqueue(){
        wp_enqueue_style('bootstrap','https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', 'array()', '5.0.0', 'all');
        /*wp_enqueue_script('jquery','https://code.jquery.com/jquery-3.3.1.slim.min.js', 'array()', '1.0.0', true);
        wp_enqueue_script('popper','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', 'array()', '1.0.0', true);
        wp_enqueue_script('bootstrapJS','https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', 'array()', '1.0.0', true);*/
        wp_enqueue_style('customStyle', get_template_directory_uri().'/css/next2.css','array()', '1.0.0', 'all');
        wp_enqueue_style('customStyle2', get_template_directory_uri().'/css/next2res.css', 'array()', '1.0.0', 'all');
        wp_enqueue_script('customJS', get_template_directory_uri().'/js/next2.js', 'array()','1.0.0', true);
    };

    add_action('wp_enqueue_scripts', 'next2_script_enqueue');

    function next2_theme_setup(){
        add_theme_support('menus');
    
    //  register_nav_menu('name_of_the menu', 'description');
        register_nav_menu('landing_page_primary', 'Home page Main Navigation');
    
    };
    add_action('init', 'next2_theme_setup');
    
    add_theme_support('custom-background');
    add_theme_support('custom-header');
    add_theme_support('post-thumbnails');
    add_theme_support('post-formats', array('aside','image','video','gallery','link','quote','audio'));
    add_image_size('contact_thumbnail', 700, 500, true);
    add_image_size('blog_thumbnail', 500, 350, true);
?>