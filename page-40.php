<?php get_header();?>
<?php
if(have_posts()):
    while (have_posts()):
        the_post();?>
<div class="contact-wrapper" style="background-image:url('<?php echo the_post_thumbnail_url(); ?>')">
        <?php 
    endwhile;
endif;
?>
<?php
$args = array(
    'cat'=>'29',
    'posts_per_page' => '1',
    'post_type' => 'post',
    'post_status' => 'publish'
);
$query = new WP_Query($args);
if($query->have_posts()):
    while($query->have_posts()):
        $query->the_post();?>
<div class="contact-content-wrapper shadow-lg">
<div class="contact-content-title text-center">
<h4><?php the_title(); ?></h4>
</div><!--title-->
<div class="contact-content-thumbnail text-center" style="background-image:url(<?php echo the_post_thumbnail_url();?>)">
<?php the_post_thumbnail('contact_thumbnail');?>
</div><!--contact content thumbnail-->
<!--div class="contact-info-wrapper">
    <img src="http://localhost/wordpress/wp-content/uploads/2018/12/information-2.png" class="float-right contact-info-icon"/><br>
</div-->
<div class="contact-content-submenu-wrapper text-center">
<div class="row">
    <div class="col-sm-4 submenu-item-wrapper" id="music">
    <img src="http://localhost/wordpress/wp-content/uploads/2018/12/play-button.png" class="contact-submenu-icon" />
        <div class="submenu-caption-wrapper">
        <p class="submenu-caption">Music</p>
        <p class="submenu-caption-subtext">My music is contagious.<br> be aware.</p>
        </div>
    </div><!--col 1-->
    <div class="col-sm-4 submenu-item-wrapper" id="webDevelopment">
        <img src="http://localhost/wordpress/wp-content/uploads/2018/12/programming.png" class="contact-submenu-icon" />
        <div class="submenu-caption-wrapper">
        <p class="submenu-caption">Web Development</p>
        <p class="submenu-caption-subtext">Meh. Mediocre at best.</p>
        </div>
    </div><!--col 2-->
    <div class="col-sm-4 submenu-item-wrapper" id="blog">
    <img src="http://localhost/wordpress/wp-content/uploads/2018/12/writing.png" class="contact-submenu-icon"/>
        <div class="submenu-caption-wrapper">
        <p class="submenu-caption">Blog</p>
        <p class="submenu-caption-subtext">kaf;lk;lk</p>
        </div>
    </div><!--col 3-->
</div><!--row-->
</div><!--submenu wrapper-->
<?php endwhile;
endif;?>
<?php wp_footer();?>
<?php get_footer();?>
</div>